var logInStatus = 0;
var signUpStatus = 0;

var main = function() {
  /* Push the body and the nav over by 285px over */
  $('.sign-up-btn').click(function() {
    if (signUpStatus == 0 && logInStatus == 0) {
      signUpStatus = 1;
      $('.sign-up-form').animate({
        bottom: "-70%" /* mindre siffra gör formuläret högre */
      }, 200);
    }

    else if (signUpStatus == 0 && logInStatus == 1 ) {
      signUpStatus = 1;
      logInStatus = 0;
      $('.sign-up-form').animate({
        bottom: "-70%" /* mindre siffra gör formuläret högre */
      }, 200);

      $('.log-in-form').animate({
        bottom: "-110%" /* gömmer formuläret utanför vy */
      }, 200);
    }

    else if (signUpStatus == 1) {
      signUpStatus = 0;
      $('.sign-up-form').animate({
        bottom: "-110%" /* mindre siffra gör formuläret högre */
      }, 200);
    }
  });

  $('.log-in-btn').click(function() {
    if (logInStatus == 0 && signUpStatus == 0){
      logInStatus = 1;
      $('.log-in-form').animate({
        bottom: "-70%" /* mindre siffra gör formuläret högre */
      }, 200);
    }

    else if (signUpStatus == 1 && logInStatus == 0 ) {
      signUpStatus = 0;
      logInStatus = 1;
      $('.sign-up-form').animate({
        bottom: "-110%" /* mindre siffra gör formuläret högre */
      }, 200);

      $('.log-in-form').animate({
        bottom: "-70%" /* gömmer formuläret utanför vy */
      }, 200);
    }

    else if (logInStatus == 1){
      logInStatus = 0;
      $('.log-in-form').animate({
        bottom: "-110%" /* mindre siffra gör formuläret högre */
      }, 200);
    }
  });

  $('.close').click(function() {
    logInStatus = 0;
    signUpStatus = 0;
    $('.log-in-form').animate({
      bottom: "-110%" /* gömmer formuläret utanför vy */
    }, 200);

    $('.sign-up-form').animate({
      bottom: "-110%" /* gömmer formuläret utanför vy */
    }, 200);
  });

};


$(document).ready(main);