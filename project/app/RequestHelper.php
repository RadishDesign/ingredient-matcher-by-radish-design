<?php

namespace App;

class RequestHelper
{
	public static function sendRequest($api, $data) {
		$url = 'http://wcf.help4dinner.com/wrapper/wrapperSL.svc/'.$api;
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => 'Fett cooool',
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_HTTPHEADER => array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data)                                                                       
				)
			));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);

		return json_decode($resp);
	}

}
