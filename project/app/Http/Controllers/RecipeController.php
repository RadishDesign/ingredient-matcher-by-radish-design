<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\RequestHelper;
use Illuminate\Http\Request;
class RecipeController extends BaseController
{
	// Returns a specific recipe with idnumber, $id. If the user is logged in, green marks will be shown next to the ingredients the user has at home.
	public function show($id) {
		// If logged in:
		if (\Cookie::has('userid')) {
			$userid = \Cookie::get('userid');
			$obj = RequestHelper::sendRequest('FetchRecipeDetail', '{"ResultFormatType":"Json", "UserID":"'.$userid.'", "RecipeId":"'. $id .'", "LanguageId":"1", "Measurement":"US", "IsChangeUnit":false}');
			}
		else {
			$obj = RequestHelper::sendRequest('FetchRecipeDetail', '{"ResultFormatType":"Json", "RecipeId":"'. $id .'", "LanguageId":"1", "Measurement":"US", "IsChangeUnit":false}');
		}
		return view('recipe.show')->with('recipe', $obj);
	}

	// Returns a list with all recipes.
	// $filter tells what category the recipes is filtered by, food, drink or dessert.
	public function list($filter) {
		$list = RequestHelper::sendRequest('GetRecipes', '{ "ResultFormatType":"Json", "MyIngredients":"No", "MyRecipesBookmarked":"No", "RecipeType":"'.$filter.'"}');
		$mine = false;
		return \View::make('recipe.list', compact('list', 'mine'));
	}

	// Returns a list of all recipes filtered by the ingredients available at home if logged in, otherwise the site redirects to the splashscreen.
	// $filter is the same as in the list function.
	public function mine($filter) {
		// If logged in:
		if (\Cookie::has('userid')) {
			$userid = \Cookie::get('userid');
			$list = RequestHelper::sendRequest('GetRecipes', '{ "ResultFormatType":"Json", "UserID":"'.$userid.'", "MyIngredients":"Yes", "MyRecipesBookmarked":"No", "RecipeType":"'.$filter.'"}');
			$mine = true;
			return \View::make('recipe.list', compact('list', 'mine'));
		}
		else {
			return redirect('/splashscreen');
		}
	}
}