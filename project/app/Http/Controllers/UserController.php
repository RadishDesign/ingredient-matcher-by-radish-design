<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\RequestHelper;
use Illuminate\Http\Request;

class UserController extends Controller
{
	// Encrypts password and attempts to log in user. If succeeded, the site redirects to current page with a cookie with the userid, otherwise the welcome screen is displayed with an error message.
    public function login(Request $request) {
		$email = $request->input('email');
		$pw = $request->input('password');
		$encr_pw = RequestHelper::sendRequest('EncryptPassword', '{"UserPassword":"' . $pw . '"}');
		$epw = $encr_pw->EncryptedPassword;
		$obj = RequestHelper::sendRequest('loginUser', '{"ResultFormatType":"json", "UserEmail":"'. $email . '", "UserPassword":"' . $epw . '", "LoginApp":1} ');
		if ($obj->UserId == 0) {
			return view('welcome')->with('info', $obj);
		}
		else {
			$userid = $obj->UserId;
			return back()->withCookie('userid', $userid, 30);
		}
		
	}
}
