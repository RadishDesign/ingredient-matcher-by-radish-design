<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\RequestHelper;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
	// Returns a list of all the ingredients available at IM if logged in, otherwise the site redirects to the splashcreen.
    public function list(Request $request) {
    	// If logged in:
    	if (\Cookie::has('userid')) {
    		$userid = $request->cookie('userid');
			$obj = RequestHelper::sendRequest('ShowIngredients','{ "ResultFormatType":"Json", "IngrCategoryId":"", "IngrStorageId":"1,2,3,4", "IngredientAvailability":"All",  "IngredientUsage":"All",  "UserId":"' . $userid . '", "LanguageId":"1" } ');
			return view('ingredients.listingredients')->with('ingredients', $obj);
    	}
    	else {
    		return redirect('/splashscreen');
    	}
	}

	// Returns a list of all the ingredients a user has at home if logged in, otherwise the site redirects to the splashscreen.
	public function listMine(Request $request) {
		// If logged in:
		if (\Cookie::has('userid')) {
			$userid = $request->cookie('userid');
			$obj = RequestHelper::sendRequest('ShowIngredients','{ "ResultFormatType":"Json", "IngrCategoryId":"", "IngrStorageId":"1,2,3,4", "IngredientAvailability":"All",  "IngredientUsage":"OnlyMine",  "UserId":"' . $userid . '", "LanguageId":"1" } ');
			return view('ingredients.listingredients')->with('ingredients', $obj);
		}
		else {
			return redirect('/splashscreen');
		}
	}

	// Updates the ingredients a user has at home and returns a list with all ingredients if logged in, otherwise the site redirects to the splashscreen.
	public function update(Request $request) {
		// If logged in:
		if (\Cookie::has('userid')) {
			$userid = $request->cookie('userid');
			$all_ingredients = RequestHelper::sendRequest('ShowIngredients','{ "ResultFormatType":"Json", "IngrCategoryId":"", "IngrStorageId":"1,2,3,4", "IngredientAvailability":"All",  "IngredientUsage":"All",  "UserId":"' . $request->cookie('userid') . '", "LanguageId":"1" } ');
			$checked_ingredients = $request->input('ingredients');

			$updated_ingredients = array();
			foreach ($checked_ingredients as $i) {
				$updated_ingredients[$i] = true;
			}

			$objectIngrListStr = "";

			foreach ($all_ingredients->ListIngredients as $ing) {
				if (!is_null($ing->UserIngrAvailability)) {
					if (isset($updated_ingredients[$ing->IngredientId])) {
						$objectIngrListStr = $objectIngrListStr . '{ "IngrId":"' . $ing->IngredientId .'", "UserIngrAvailability":"'.$ing->UserIngrAvailability.'", "IngrMarked":"true" },'; 
					}
					else {
						$objectIngrListStr = $objectIngrListStr . '{ "IngrId":"' . $ing->IngredientId .'", "UserIngrAvailability":"'.$ing->UserIngrAvailability.'", "IngrMarked":"false" },';
					}
				}	
			}

			// Removes comma
			$objectIngrList = substr($objectIngrListStr, 0, -1);

			RequestHelper::sendRequest('UploadUserIngredients', '{ "UserId":"'. $userid .'", "UploadedIngredientsList": [ '. $objectIngrList.' ] }');

			return back();
		}

		else {
			return redirect('/splashscreen');
		}
		
	}
		
}