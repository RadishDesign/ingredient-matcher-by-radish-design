<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('splashscreen');
});
Route::get('/about_us', function () {
	return view('aboutus');
});
Route::get('/splashscreen', function () {
	return view('splashscreen');
});
Route::get('/ingredients', 'IngredientController@list');
Route::get('/ingredients/mine', 'IngredientController@listMine');
Route::get('/recipe/all/{filter}', 'RecipeController@list');
Route::get('/recipe/mine/{filter}', 'RecipeController@mine');
Route::get('/recipe/show/{id}', 'RecipeController@show');
Route::post('/recipe/search', 'RecipeController@search');
Route::post('/login', 'UserController@login');
Route::post('/ingredients/update', 'IngredientController@update');
Route::get('/logout', function () {
	$cookie = Cookie::forget('userid');
	return redirect('/')->withCookie($cookie);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
