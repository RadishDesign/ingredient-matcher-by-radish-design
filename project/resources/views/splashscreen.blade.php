@extends('master')

@section('content')

<!-- The carousel -->
    <div class="slider">

      	<!-- Slide 1 -->
      	<div class="slide active-slide">
        	<div class="container">
          		<div id="jumbotron-carousel" class="jumbotron">

            		<div class="row vertical-align">
              			<div class="prev col-xs-1">
                			&#x2039;
              			</div>

              			<div class="col-xs-10">
                			<div class="container">
                  				<div class="row">

                    				<div class="col-xs-12 col-md-5">
                      					<p>Ever been tired of looking at recipes with all these ingredients you don't have at home and spending time browsing and browsing for ages for dishes you can cook?</p>
                    				</div>

                    				<div class="col-xs-12 col-md-7">
                      					<img src="/Empty.jpg" class="img-responsive center-block splashpic">
                    				</div>
                 				
                 				</div>

                  				<!-- Dots to show what slide is showing -->
                  				<div class="row">
                    				<div id="dot_area" class="nav-dots col-xs-12">
                      					<label class="nav-dot" id="active-dot"></label>
                     					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                    				</div>
                  				</div>

                  				<!-- Sign Up and Log in buttons-->
                  				<!-- Pressing these open the sign up/log in form -->
                  				<div class="row">
                    				<div class="col-xs-12 center-block">
                      
                      					<div id="sign_up_button_area" class="text-center">
                        					<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
                        					<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
                      					</div>
                      
                    				</div>
                  				</div>

                			</div>
              			</div>

              			<div class="next col-xs-1" style="padding-left: 0px;">
                			&#x203a;
              			</div>
              		</div>

          		</div>      
        	</div>   
      	</div>

      	<!-- Slide 2 -->
      	<div class="slide">
        	<div class="container">
          		<div id="jumbotron-carousel" class="jumbotron">

            		<div class="row vertical-align">
              			<div class="prev col-xs-1">
                			&#x2039;
              			</div>

              			<div class="col-xs-10">
                			<div class="container">
                  				<div class="row">

                    				<div class="col-xs-12 col-md-5">
                      					<p>What if there was a way for you to actually see recipes that you can make without having to stop by the grocery store?</p>
                    				</div>

                    				<div class="col-xs-12 col-md-7">
                      					<img src="/Shopping.jpg" class="img-responsive center-block splashpic">
                    				</div>

                  				</div>
                  
                  				<!-- Dots to show what slide is showing -->
                  				<div class="row">
                    				<div id="dot_area" class="nav-dots col-xs-12">
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot" id="active-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                    				</div>
                  				</div>

                  				<!-- Sign Up and Log in buttons-->
                  				<!-- Pressing these open the sign up/log in form -->
                  				<div class="row">
                    				<div class="col-xs-12 center-block">
                      
                      					<div id="sign_up_button_area" class="text-center">
                        					<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
                        					<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
                      					</div>
                      
                    				</div>
                  				</div>

                			</div>
              			</div>

              			<div class="next col-xs-1" style="padding-left: 0px;">
               				&#x203a;
              			</div>
              		</div>

          		</div>      
        	</div>
      	</div>

      	<!-- Slide 3 -->
      	<div class="slide">
        	<div class="container">
          		<div id="jumbotron-carousel" class="jumbotron">

            		<div class="row vertical-align">
              			<div class="prev col-xs-1">
                			&#x2039;
              			</div>

              			<div class="col-xs-10">
                			<div class="container">
                  				<div class="row">

                    				<div class="col-xs-12 col-md-5">
                      					<p>On IngredientMatcher, you are able to enter those ingredients you have at home and match for recipes that you can cook right here and right now.</p>
                    				</div>

                    				<div class="col-xs-12 col-md-7">
                      					<img src="/Cooking1.jpg" class="img-responsive center-block splashpic">
                    				</div>
                  				
                  				</div>

                  				<!-- Dots to show what slide is showing -->
                  				<div class="row">
                    				<div id="dot_area" class="nav-dots col-xs-12">
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot" id="active-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                      					<label class="nav-dot"></label>
                    				</div>
                  				</div>

                  				<!-- Sign Up and Log in buttons-->
                  				<!-- Pressing these open the sign up/log in form -->
                  				<div class="row">
                    				<div class="col-xs-12 center-block">
                      
                      					<div id="sign_up_button_area" class="text-center">
                        					<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
                        					<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
                      					</div>
                      
                    				</div>
                  				</div>

                			</div>
              			</div>

              			<div class="next col-xs-1" style="padding-left: 0px;">
                			&#x203a;
              			</div>
            		</div>

        		</div>      
    		</div>
		</div>

      	<!-- Slide 4 -->
      	<div class="slide">
        	<div class="container">
          		<div id="jumbotron-carousel" class="jumbotron">

	            	<div class="row vertical-align">
	              		<div class="prev col-xs-1">
	                		&#x2039;
	              		</div>

	              		<div class="col-xs-10">
	                		<div class="container">
	                  			<div class="row">

	                    			<div class="col-xs-12 col-md-5">
	                      				<p>Now you won't need to plan your lunches and dinners hours or days in advanced. ! Feel free to be spontaneous! Forget the frustration of viewing recipes with ingredients that you don't have at home. Skip the extra shopping for excessive ingredients...</p>
	                    			</div>

	                    			<div class="col-xs-12 col-md-7">
	                      				<img src="/Time.jpg" class="img-responsive center-block splashpic">
	                    			</div>

	                  			</div>

	                  			<!-- Dots to show what slide is showing -->
	                  			<div class="row">
	                    			<div id="dot_area" class="nav-dots col-xs-12">
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot" id="active-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                    			</div>
	                  			</div>

	                  			<!-- Sign Up and Log in buttons-->
	                  			<!-- Pressing these open the sign up/Log in form -->
	                  			<div class="row">
	                    			<div class="col-xs-12 center-block">
	                      
	                      				<div id="sign_up_button_area" class="text-center">
	                        				<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
	                        				<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
	                      				</div>
	                      
	                    			</div>
	                  			</div>

	                		</div>
	              		</div>

	              		<div class="next col-xs-1" style="padding-left: 0px;">
	                		&#x203a;
	              		</div>
	            	</div>

        		</div>      
    		</div>
    	</div>

	    <!-- Slide 5 -->
	    <div class="slide">
	        <div class="container">
	          	<div id="jumbotron-carousel" class="jumbotron">

	            	<div class="row vertical-align">
	              		<div class="prev col-xs-1">
	                		&#x2039;
	              		</div>

	              		<div class="col-xs-10">
	                		<div class="container">
	                  			<div class="row">

	                    			<div class="col-xs-12 col-md-5">
	                      				<p>...when you can cook recipes on the instant! Just go to "Ingredients" in the top menu and enter your ingredients in order to match for recipes that you can cook. Click your way through hundreds of versatile recipes.</p>
	                    			</div>

	                    			<div class="col-xs-12 col-md-7">
	                      				<img src="/Dishes.jpg" class="img-responsive center-block splashpic">
	                    			</div>

	                  			</div>

	                  			<!-- Dots to show what slide is showing -->
	                  			<div class="row">
	                    			<div id="dot_area" class="nav-dots col-xs-12">
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot"></label>
	                      				<label class="nav-dot" id="active-dot"></label>
	                      				<label class="nav-dot"></label>
	                    			</div>
	                  			</div>

	                  			<!-- Sign Up and Log in buttons-->
	                  			<!-- Pressing these open the sign up/Log in form -->
	                  			<div class="row">
	                    			<div class="col-xs-12 center-block">
	                      
	                      				<div id="sign_up_button_area" class="text-center">
	                        				<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
	                        				<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
	                      				</div>
	                      
	                    			</div>
	                  			</div>

	                		</div>
	              		</div>

	              		<div class="next col-xs-1" style="padding-left: 0px;">
	                		&#x203a;
	              		</div>             
	            	</div>

	          	</div>      
	        </div>
	    </div>

	    <!-- Slide 6 -->
	    <div class="slide">
	        <div class="container">
	          	<div id="jumbotron-carousel" class="jumbotron">

	            	<div class="row vertical-align">
	              		<div class="prev col-xs-1">
	                		&#x2039;
	              		</div>

	              		<div class="col-xs-10">
	                		<div class="container">
	                  			<div class="row">

	                    			<div class="col-xs-12 col-md-5">
	                      				<p>Join us today and start cooking immediately with the ingredients you have at home with IngredientMatcher!</p>
	                    			</div>

	                    			<div class="col-xs-12 col-md-7">
	                      				<img src="/Cooking2.jpg" class="img-responsive center-block splashpic">
	                    			</div>

	                  			</div>

		                  		<!-- Dots to show what slide is showing -->
		                  		<div class="row">
		                    		<div id="dot_area" class="nav-dots col-xs-12">
		                      			<label class="nav-dot"></label>
		                      			<label class="nav-dot"></label>
		                      			<label class="nav-dot"></label>
		                      			<label class="nav-dot"></label>
		                      			<label class="nav-dot"></label>
		                      			<label class="nav-dot" id="active-dot"></label>
		                    		</div>
		                  		</div>

		                  		<!-- Sign Up and Log in buttons-->
		                  		<!-- Pressing these open the sign up/log in form -->
		                  		<div class="row">
		                    		<div class="col-xs-12 center-block">
		                      
		                      			<div id="sign_up_button_area" class="text-center">
		                        			<button type="button" class="btn btn-success sign-up-btn">Sign Up</button>
		                        			<button type="button" class="btn btn-success log-in-btn col-md-offset-1">Log In</button>
		                      			</div>
		                      
		                    		</div>
		                  		</div>

	                		</div>
	              		</div>

	              		<div class="next col-xs-1" style="padding-left: 0px;">
	                		&#x203a;
	              		</div>              
	            	</div>

	          	</div>      
	        </div>
	    </div>

	</div>


	 <br>

	 <!-- Log in form -->
	 <div class="jumbotron log-in-form">
	    <div class="container">
	      	<div class="row">
	        	<div id="sign_up_form_log_in col-xs-12">
	          		<div class="close">
	            		<p>close</p>
	          		</div>

	          		<form class="form-horizontal" role="form" method="post" action="/login" accept-charset="UTF-8">
	            		<div class="row">
	              			<div class="col-xs-2 col-xs-offset-3">
	                			<label id="form_details" class="control-label" for="email">Email:</label>
	              			</div>

	              			<div class="col-xs-4">
	                			<input id="form_details" type="email" name="email" class="form-control" id="email" placeholder="Enter email address" required>
	              			</div>
	            		</div>

		            	<div class="row">
		              		<div class=" col-xs-2 col-xs-offset-3">
		                		<label id="form_details" class="control-label" for="password">Password:</label>
		              		</div>

		              		<div class="col-xs-4">
		                		<input id="form_details" type="password" name="password" class="form-control" id="password" placeholder="Enter password">

		                  		

		                	</div>
		              	</div>

		              	<div class="row">
		                	<div class="col-xs-offset-6 col-xs-6">
		                  		<button id="form_button" type="submit" class="btn btn-success btn-lg">Log In</button>
		                	</div>
		              	</div>
	            	</form>

	        	</div>
	    	</div>
		</div>
	</div>

	<!-- End of form -->

	 <br>

	<!-- Sign up form -->
	<div class="jumbotron sign-up-form">
	    <div class="container">
	      	<div class="row">
	        	<div id="sign_up_form_log_in col-xs-12">
	          		<div class="close">
	            		<p>close</p>
	          		</div>

	          		<form class="form-horizontal" role="form" method="post" action="login" accept-charset="UTF-8">
	            		<div class="row">
	              			<div class=" col-xs-2 col-xs-offset-3">
	                			<label id="form_details" class="control-label" for="email">Email:</label>
	              			</div>
	              			
	              			<div class="col-xs-4">
	                			<input id="form_details" type="email" class="form-control" id="email" placeholder="Enter email address" required>
	              			</div>
	            		</div>

	            		<div class="row">
	              			<div class=" col-xs-2 col-xs-offset-3">
	                			<label id="form_details" class="control-label" for="password">Password:</label>
	              			</div>
	              			
	              			<div class="col-xs-4">
	                			<input id="form_details" type="password" class="form-control" id="password" placeholder="Enter password">
	              			</div>
	            		</div>

	            		<div class="row">
	              			<div class=" col-xs-2 col-xs-offset-3">
	                			<label id="form_details" class="control-label" for="measurement">Preferred measurement system:</label>
	              			</div>

	              			<div class="help-block col-xs-4">                    
	                			<div id="metricbtns" class="btn-group" role="group">
	                 				<button id="form_details" type="button" class="btn btn-default">US</button>
	                 				<button id="form_details" type="button" class="btn btn-default">Metric</button>
	               				</div>
	             			</div>
	           			</div>

	           			<div class="row">
	            			<div class="col-xs-offset-6 col-xs-6">
	              				<button id="form_button" type="button" class="btn btn-success btn-lg">Join</button>
	            			</div>
	          			</div>
	        		</form>

	      		</div>
	    	</div>
	  	</div>
	 </div>
	 <!-- End of form -->

<script src="/app.js"></script>
<script src="/main.js"></script>

@endsection