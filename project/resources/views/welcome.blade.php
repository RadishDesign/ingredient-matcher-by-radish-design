@extends('master')

@section('content')

<div class="container">
     <div class="jumbotron text-center">

        <div class="row">

            <h1 class="col-md-12">Login failed</h1>
            <p>{{ $info->ErrorText }}</p>
            
			<div class="col-xs-12">
				<div class="btn-group col-md-4 col-md-offset-4">
					<p align="center"><a href="/"><button type="button" class="btn btn-success btn-lg">Return to start page</button></a></p>
				</div>
			</div>
   
        </div>

    </div>
</div>

@endsection