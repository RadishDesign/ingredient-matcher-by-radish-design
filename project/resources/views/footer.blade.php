@yield('blank')

	<footer class="footer">
		<div id="footer" class="container">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-2 col-md-3 col-md-offset-1 hidden-sm hidden-xs">
					<img id="footerlogo" src="http://www.ingredientmatcher.com/Content/themes/PreLaunchPage/Images/logo.png">
				</div>

				<div id="footer_links" class="col-xs-3 col-md-3 navbar-text">
					<ul align="center" class="list-unstyled">
						<li><a href="/recipe/all/1" style="color: #000000">Recipes</a></li>
						<li><a href="/ingredients" style="color: #000000">Ingredients</a></li>
						<li><a href="http://blog.ingredientmatcher.com/" style="color: #000000">Blog</a></li>
					</ul>
				</div>

				<div id="footer_links" class="col-xs-3 col-md-3 navbar-text">
					<ul align="center" class="list-unstyled">
						<li><a href="/about_us" style="color: #000000">About Us</a></li>
						@if (!Cookie::has('userid'))
						<li><a href="/splashscreen" style="color: #000000">Log In</a></li>
						<li><a href="/splashscreen" style="color: #000000">Sign Up</a></li>
						@endif
					</ul>                           
				</div>

			</div>
		</div>

		<div id="footer_copyright" class="row">
			<div class="col-xs-12 col-md-12 row">
				<p style="margin: 0px;" align="center">Copyright © 2016 IngredientMatcher</p>
			</div>
		</div>	
	</footer>
</body>
</html>