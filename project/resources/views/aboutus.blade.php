@extends('master')
@extends('footer')

@section('content')

<div class='container'>
	<div class='jumbotron text'>

		<!-- Responsive image -->
		<div class="row">
			<!-- Here any text about the company can be added -->
		  	<div id="about_us_group" class="col-md-12">
		  		<h3>About Us</h3>
		  		<div id="description_about" class="textaboutus">
		  			<p>
		  				Find new recipes that you can make with the ingredients you have at home. Ingredient Matcher is like a good recipe site, but you can find recipes based on what you can make now.
		  			</p>
		  		</div>
			</div>

			<div id="motivation_group" class="col-md-12">
		  		<h3>Motivation</h3>
		  		<div id="description_motivation" class="textaboutus">
		  			<p>
		  				Imagine that you are on your way home from work and thinking about what to cook for dinner. You probably don't know by heart what ingredients you have at home or what recipes are possible to make from them. When you arrive at home you want to start cooking right away and not spend precious time searching for recipes that you in most cases realize that you are missing vital ingredients for. On the other hand you still want to try something new or get help in finding which of your favorite recipes you should cook tonight instead of just mixing some random stuff together.
		  			</p>
		  			<p>
						That is why I am starting up a company with a service where you can match the ingredients you have at home with the recipes you can make.
					</p>
					<p>
						This recipe site will be different than all other recipe sites in three ways:
					</p>
					<p>
						It will show you the recipes you can make based on what you have at home.
						It will contain recipes from every country in the world (starting with the "national dish" of every country).
						It will in the future be available in all the biggest languages in the world (each recipe available in all languages).
						I have listed what the national dish is in all countries in the world and I have also started a campaign to get "country chefs" creating recipes for those national dishes. I have received 65 recipes/countries so far but will get more coming in soon.
					</p>
					<p>
						The service will be launched later this year and will then be available in both English and Swedish to begin with. It will contain the national dish of every country in the world (with the ambition to have one person from every country that have created the recipe for it). At the same time it will be possible to quickly and easily copy your favorite recipes in to IngredientMatcher so that the recipe collection will grow by the users.
					</p>
					<p>
						The service will be free to start with and free of ads, but when the recipe collection has grown enough then users who want to match their ingredients with recipes have to pay a small subscription fee.
					</p>
					<p>
						Common situations where IngredientMatcher can help:
					</p>
					<p>
						When you want to cook something now but can't find the inspiration
						When you don't want to view recipe after recipe only to find out that you are missing some vital ingredients
						When you want to use up groceries that are about to expire
						When you don't want to go shopping but still want to cook something new
					</p>
		  		</div>
		  	</div>
		</div>
	</div>
</div>		

@endsection
@section('blank')
@endsection