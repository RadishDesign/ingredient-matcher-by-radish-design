@extends('master')
@extends('footer')

@section('content')


<div class="container cfood">
	<div class="row">

		<div class='container col-xs-12 text-center'>
			<div id="recipe-list-area" class='jumbotron'>

				<div class="container">
					<div class="row">
						<h2 id="ingredienttitle" class="col-xs-12">Ingredients</h2> 
						<p>Mark what ingredients you have at home, then match them and see what you can cook with your ingredients.</p>
					</div>
					<div class="col-xs-12">
						<div id="signupbtn" class="btn-group col-md-4 col-md-offset-4">
							<p align="center"><a href="/recipe/mine/1"><button type="button" class="btn btn-success btn-lg">Match Ingredients</button></a></p>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="row">

						<form class="form" role="form" method="post" action="/ingredients/update" accept-charset="UTF-8" id="update-ingr">

							<div class="col-xs-12">
								<ul id="filterlist" class="list-group row ingredientslist center-block">
									<li id="ing" class="list-group-item col-xs-2 col-sm-2"><h4>Show:</h4></li>
									<li id="ing" class="list-group-item col-xs-10 col-sm-3"><div class="btn-group">

											<div id="ingbtns" class="btn-group" role="group">
												<a href="/ingredients">
												<button type="button" class="btn btn-default">All</button></a>
												<a href="/ingredients/mine">
												<button type="button" class="btn btn-default">My Ingredients</button></a>
											</div>
											
									</div></li>
									<li id="ing" class="list-group-item col-xs-4"><div class="btn-group col-xs-1 col-md-1 col-md-offset-7">
										<button type="submit" class="btn btn-default btn-md save" onclick="highlight(document.getElementById('save-text'));">Save Changes</button>
									</div></li>
								</ul>
							</div>

							<ul id="inglist" class="list-group row ingredientslist center-block">

								@foreach ($ingredients->ListIngredients as $ing)
								@if (!is_null($ing->UserIngrAvailability))
								@if ($ing->IngrMarked )
									<li id="ing" class="list-group-item col-xs-4"><div class="checkbox"><label><input type="checkbox" name="ingredients[]" value="{{$ing->IngredientId}}" checked="checked">&nbsp; {{$ing->IngredientName}}</label></div></li>
								@else
									<li id="ing" class="list-group-item col-xs-4"><div class="checkbox"><label><input type="checkbox" name="ingredients[]" value="{{$ing->IngredientId}}">&nbsp; {{$ing->IngredientName}} </label></div></li>
								@endif
								@endif
								@endforeach

							</ul>

						</form>

					</div>
				</div>

			</div>
		</div>

	</div>
</div>

@endsection

@section('blank')
@endsection