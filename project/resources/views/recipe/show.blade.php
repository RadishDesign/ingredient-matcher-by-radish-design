@extends('master')
@extends('footer')

@section('content')


<div class="container">
     <div class="jumbotron text-center">

        <!-- Responsive image -->
        <div id="picture-container" class="row">
            <h1 id="recipe_title" class="col-md-12">{{ $recipe->RecipeTitle }}</h1>
            @if (count($recipe->ListRecipeImageBE) > 0)
            <div id="exempel"class="img-responsive col-md-6 col-md-offset-3 thumbnail">
                <img src="{{ $recipe->ListRecipeImageBE[0]->ImageUrl}}" class="center-block img-responsive">
            </div>
            @endif
        </div>

        <div class="row">
            <div id="ingredient-container" class="col-md-4">

                <h2 id="ingredienttitle">Ingredients</h2> 

                <ul id="ingredients_list" class="list-group row ingredientslist">
                
                @foreach ($recipe->IngredientsList as $ing)

                    @if ($ing->IsMarked)
                    <li class="list-group-item">
                        @if (is_null($ing->FractionDenominator))
                        <h4> 
                            {{$ing->IngrQuantity}} {{$ing->UnitName}} {{$ing->IngrName }}
                            <span class="glyphicon glyphicon-ok" style= "color:green"></span>
                        </h4>
                        @else
                        <h4>
                            {{$ing->FractionNumerator}}/{{$ing->FractionDenominator}} {{$ing->UnitName}} {{$ing->IngrName }}
                            <span class="glyphicon glyphicon-ok" style= "color:green"></span>
                        <h4>
                        @endif
                    </li>

                    @else
                    <li class="list-group-item">
                        @if (is_null($ing->FractionDenominator))
                        <h4>
                            {{$ing->IngrQuantity}} {{$ing->UnitName}} {{$ing->IngrName }} 
                        </h4>
                         @else
                        <h4>
                            {{$ing->FractionNumerator}}/{{$ing->FractionDenominator}} {{$ing->UnitName}} {{$ing->IngrName }}
                        <h4>
                        @endif
                    </li>

                    @endif

                @endforeach

                </ul>

            </div>

            <div id="instruction-container" class="col-md-8">
                <h2 id="instructions">Instructions</h2> 
                <div class= "instructiontext">
                    <h4>
                        <ul id="instruction_elements" style="list-style-type: none;">
                          
                        @foreach (explode("\n", $recipe->Preparation) as $text)

                        @if (starts_with($text, '!'))
                            <h3><li class="linespacing">{{ substr($text, 1) }}</li></h3>
                            
                        @else 
                            <li class="linespacing"> {{ $text }}</li></br>
            
                        @endif
                        @endforeach 
          
                        </ul>
                    </h4> 
                </div>
            </div>
        </div>

        <div class="row">
            <div id="description-container" class="col-md-12">
                @if ( !is_null($recipe->UploaderComment) )
                <div class="row">
                    <div id="description_text" class="col-md-12 descriptiontext">
                        <blockquote>
                            {{ $recipe->UploaderComment }}
                            <div id="quote" align="right">&#x201d;</div>
                        </blockquote>
                    </div>
                </div>
                @endif

                <div class="row text-center">
                    <div style="font-size: 16px;">
                        <b>Source: </b> {{$recipe->SourceOfRecipe }}<br>
                        <b>Uploaded by: </b> {{$recipe->UserName}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('blank')
@endsection