@extends('master')
@extends('footer')

@section('content')

<div class="container cfood">
	<div class="row">

		<!-- Recipes -->
		<div class='container col-xs-12 text-center'>
			<div id="recipe-list-area" class='jumbotron'>

				<!-- Title -->
				<div class="container">
					<div class="row">
						<h2 id="ingredienttitle" class="col-xs-12">Recipes</h2> 
					</div>
				</div>
		
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<ul id="filterlist" class="list-group row ingredientslist center-block">
								<li id="ing" class="list-group-item col-xs-4 col-md-2">
									<h4>Categories:</h4>
								</li>

								<li id="ing" class="list-group-item col-xs-6.1 col-md-4">
									<div class="btn-toolbar" role="toolbar">
										<div id="ingbtns" class="btn-group" role="group">
											@if ($mine)
											<a href="/recipe/mine/1">
											<button type="button" class="btn btn-default">Food</button></a>
											<a href="/recipe/mine/2">
											<button type="button" class="btn btn-default">Dessert</button></a>
											<a href="/recipe/mine/3">
											<button type="button" class="btn btn-default">Drink</button></a>
											

											@else
											<a href="/recipe/all/1">
											<button type="button" class="btn btn-default">Food</button></a>
											<a href="/recipe/all/2">
											<button type="button" class="btn btn-default">Dessert</button></a>
											<a href="/recipe/all/3">
											<button type="button" class="btn btn-default">Drink</button></a>
											
											@endif
										</div>
									</div>
								</li>

								<li id="ing" class="list-group-item col-xs-4 col-md-2 col-md-offset-1">
									<h4>Sort by:</h4>
								</li>

								<li id="ing" class="list-group-item col-xs-8 col-sm-6 col-md-2">
									<div class="btn-toolbar" role="toolbar">
										<div id="ingbtns" class="btn-group" role="group">
											<a href="/recipe/all/1">
											<button type="button" class="btn btn-default">All</button></a>
											<a href="/recipe/mine/1">
											<button type="button" class="btn btn-default">Matched</button></a>
										</div>
									</div>
								</li>

							</ul>
						</div>

						@if ($list->recipes > 0)
						@foreach ($list->recipes as $recipe) 
							<div class="col-xs-4 recipe-preview">
								<a href="/recipe/show/{{ $recipe->RecipeId}}">

									@if (strcmp($recipe->ImageUrl, "") == 0)
									<img src="https://pixabay.com/static/uploads/photo/2016/04/14/22/58/fork-1329957_960_720.jpg" class="img-responsive" alt="Picture">

									@else
									<img src="{{ $recipe->ImageUrl }}" class="img-responsive">
									
									@endif
								</a>
								<a href="/recipe/show/{{ $recipe->RecipeId}}">
									<label id="recipe-list-title">{{ $recipe->RecipeTitle }}</label>
								</a>
							</div>

						@endforeach

						@else
						<p>No recipes matched.</p>
						@endif
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

@endsection

@section('blank')
@endsection