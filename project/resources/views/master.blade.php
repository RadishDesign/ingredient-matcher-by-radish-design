<!doctype html>
<html>
<head>
	<title>Ingredient Matcher</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="/main.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<meta charset="utf-8"></meta>

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
	<!-- Navigation bar --> 
	<nav class = "navbar navbar-default navbar-fixed-top" role = "navigation">
		<div class="container-fluid">

			<div class = "navbar-header">

				<button type = "button" class = "navbar-toggle collapsed" data-toggle = "collapse" data-target = "#example-navbar-collapse">
					<span class = "sr-only">Toggle navigation</span>
					<span class = "icon-bar"></span>
					<span class = "icon-bar"></span>
					<span class = "icon-bar"></span>
				</button>

				<!-- Ingredient Match logo - leads to index -->
				<a class="navbar-brand" href="/"><img src="/logo.png"></a>

			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse text-center" id="example-navbar-collapse">
				<div class="container">
					<!-- All the parts of navbar -->
					<ul id= "filter" class="nav navbar-nav text-center">
						<li><a href="/recipe/all/1">Recipes</a></li>
						<li><a href="/ingredients">Ingredients</a></li>

						@if (!Cookie::has('userid'))
						<li><a href="/splashscreen">Sign up</a></li>
						<!-- Login dropdown -->
						<li class="dropdown">
							<a id="option_log_in" href="#" class="dropdown-toggle" data-toggle="dropdown">Login<span class="caret"></span></a>
							<ul id="login-dp" class="dropdown-menu">
								<li><div class="row">
										<div class="col-md-12">
											<!-- Login form -->
											<form class="form" role="form" method="post" action="/login" accept-charset="UTF-8" id="login-nav">
												<div class="form-group">
													<label class="sr-only" for="exampleInputEmail2">Email address</label>
													<input type="email" class="form-control" name="email" id="exampleInputEmail2" placeholder="Email address" required>
												</div>
												<div class="form-group">
													<label class="sr-only" for="exampleInputPassword2">Password</label>
													<input type="password" class="form-control" name="password" id="exampleInputPassword2" placeholder="Password" required>
												</div>
												<div class="form-group">
													<input type="submit" class="form-control" value="Sign in">
												</div>
											</form>
										</div>
								</div></li>
							</ul>
						</li>

						@else
						<li><a href="/logout">Log out</a></li>
						@endif

						<!-- Settings dropdown -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-option-horizontal"></span></a>
							<ul class="dropdown-menu">
								<li><a href="/about_us">About Us</a></li>
								<li><a href="http://blog.ingredientmatcher.com">Blog</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>

		</div>
	</nav>
	<!-- End of Navbar -->


	@yield('content')

